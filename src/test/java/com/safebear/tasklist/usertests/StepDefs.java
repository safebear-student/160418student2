package com.safebear.tasklist.usertests;

import com.safebear.tasklist.usertests.pages.TaskListPage;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class StepDefs {

    WebDriver driver;

    TaskListPage taskListPage;

    // getting the URL details
    final String DOMAIN = System.getProperty("domain");
    final String PORT = System.getProperty("port");
    final String CONTEXT = System.getProperty("context");

    @Before
    public void setUp(){
        // For running in actual browser, non-headless
        // driver = new ChromeDriver();
        String browser = System.getProperty("browser");

        switch (browser){
            case "headless":
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless");
                driver = new ChromeDriver(options);
                break;

            case "chrome":
                driver = new ChromeDriver();
                break;

            default:
                driver = new ChromeDriver();
                break;
        }

        taskListPage = new TaskListPage(driver);

        String url = DOMAIN + ":" + PORT + "/" + CONTEXT;
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @When("^a user creates a (.+)$")
    public void a_user_creates_a_task(String taskname) {
        // Write code here that turns the phrase above into concrete actions
        taskListPage.addTask(taskname);
    }

    @Then("^the (.+) appears in the list$")
    public void the_task_appears_in_the_list(String taskname) {
        // Write code here that turns the phrase above into concrete actions
        Assertions.assertThat(taskListPage.checkForTask(taskname)).isTrue();
    }
    @Given("^the following tasks are created:$")
    public void the_following_tasks_are_created(DataTable arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        throw new PendingException();
    }

    @When("^the homepage opens$")
    public void the_homepage_opens() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the following tasks appear in the list:$")
    public void the_following_tasks_appear_in_the_list(DataTable arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        throw new PendingException();
    }

    @Given("^a Configure Jenkins is in a not completed$")
    public void a_Configure_Jenkins_is_in_a_not_completed() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^a Configure Jenkins is updated to a completed$")
    public void a_Configure_Jenkins_is_updated_to_a_completed() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the Configure Jenkins is now in the completed$")
    public void the_Configure_Jenkins_is_now_in_the_completed() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @After
    public void tearDown(){
        driver.quit();
        try{
            Thread.sleep(2000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
