package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class TaskTest {

    @Test
    public void creation(){

        LocalDate localDate = LocalDate.now();

        Task task = new Task(1L, "Configure Jenkins server", localDate, false);

        Assertions.assertThat(task.getId()).isEqualTo(1L);

        //Check that the task name isnt blank
        Assertions.assertThat(task.getName()).isNotBlank();

        //Check the task name
        Assertions.assertThat(task.getName()).isEqualTo("Configure Jenkins server");

        //Check date
        Assertions.assertThat(task.getDueDate().isEqual(localDate));

        //Check it's not yet completed
        Assertions.assertThat(task.getCompleted()).isEqualTo(false);

        //Other options for assertions - JUnit Assert library
        Assert.assertEquals(task.getId(), (Long) 1L);

        //hamcrest
        Assert.assertThat(task.getId(), Matchers.equalTo(1L));
    }
}
