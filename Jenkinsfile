pipeline{

    // Which jenkins server will be running the pipeline
    agent any

    parameters{

        // tests to run

        // API
        string(name: 'apiTests', defaultValue: 'ApiTestsIT', description: 'APItests')

        //Cucumber
        string(name: 'cuke', defaultValue: 'RunCukesIT', description: 'cucumber tests')

        //general
        string(name: 'context', defaultValue: 'safebear', description: 'application context')
        string(name: 'domain', defaultValue: 'http://34.218.44.249', description: 'domain of the test environment')

        // test envrionment
        string(name: 'test_hostname', defaultValue: '34.218.44.249', description: 'hostname of the test environment')
        string(name: 'test_port', defaultValue: '8888', description: 'port of the test env')
        string(name: 'test_username', defaultValue: 'tomcat', description: 'username of tomcat')
        string(name: 'test_password', defaultValue: 'tomcat', description: 'password of tomcat server')

        //browser
        string(name: 'browser', defaultValue: 'headless', description: 'browser for our gui tests')
    }

    options{
        // this allows us to only keep the artifacts and build logs of the last 3 builds
        buildDiscarder(logRotator(numToKeepStr: '3', artifactNumToKeepStr: '3'))
    }
    //poll every minute
    triggers{
        pollSCM('* * * * *')
//        pollSCM('* 0 * * *') //Poll Mid-night
//        pollSCM('5 * * * *') // Poll every 5 minutes
//        pollSCM('H/5 * * * *') // Poll every 5 minutes, don't run if a job is running already
    }

    stages{
        stage('Build with Unit Testing'){
            steps {
                sh 'mvn clean package'
            }
            post{
                success {
                    //print a message to screen that we're archiving
                    echo 'Now archiving...'
                    archiveArtifacts artifacts: '**/target/*.war'
                }

                always{
                    junit "**/target/surefire-reports/*.xml"
                }
            }
        }

        stage('Static Analysis'){
            // run the mvn checkstyle:checkstyle command to run the static analysis.
            // Can be done in parallel to the Build and Unit Testing step

            steps{
                sh 'mvn checkstyle:checkstyle'
            }
            post{
                success{
                    checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''
                }
            }
        }

        stage('Deploy to Test'){
            // Deploy to the test environment so we can run our integration and BDD tests

            steps{
                //deploy using the cargo plugin in the pom.xml
                sh "mvn cargo:redeploy -Dcargo.hostname=${params.test_hostname} -Dcargo.servlet.port=${params.test_port} -Dcargo.username=${params.test_username} -Dcargo.password=${params.test_password}"
            }
        }

        stage('integration tests'){
            steps {
                sh "mvn -Dtest=${params.apiTests} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"

            }
            post {
                always {
                    junit "**/target/surefire-reports/*ApiTestsIT.xml"
                }
            }

        }

        stage('BDD Requirements Testing'){

            steps{
                sh "mvn -Dtest=${params.cuke} verify -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context} -Dbrowser=${params.browser}"
            }
            post{
                always{
                    publishHTML([
                            allowMissing            : false,
                            alwaysLinkToLastBuild   : false,
                            keepAll                 : false,
                            reportDir               : 'target/cucumber',
                            reportFiles             : 'index.html',
                            reportName              : 'BDD Report',
                            reportTitles            : ''])
                }
            }
        }
    }
}