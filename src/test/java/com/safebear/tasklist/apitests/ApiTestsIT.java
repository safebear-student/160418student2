package com.safebear.tasklist.apitests;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import org.junit.Before;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class ApiTestsIT {

    final String DOMAIN = System.getProperty("domain");
    final int PORT = Integer.parseInt(System.getProperty("port"));
    final String CONTEXT = System.getProperty("context");

    @Before
    public void setUp(){

        //setting the base URI details and the Port
        RestAssured.baseURI=DOMAIN;
        RestAssured.port=PORT;
        //register parser for the response as JSON as we're talking to an API
        RestAssured.registerParser("application/json", Parser.JSON);
    }

    @Test
    public void testHomePage(){
        //now we construct our URL for the /api/tasks endpoint
        get("/" + CONTEXT + "/api/tasks")
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test public void testSaveTask() {
        //Send the post request and expect the same task to be returned.
        //Don't forget to set the content type as it's a POST request

        given().contentType("application/json")
                .body("{\"name\":\"Configure Jenkins\", \"completed\":false, \"dueDate\":\"05/04/2018\"}").
                when()
                .post("/" + CONTEXT + "/api/tasks/save").
                then()
                .body("name", equalTo("Configure Jenkins"));
    }
}
